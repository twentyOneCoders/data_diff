package datadiff;

import org.junit.Test;

import static org.junit.Assert.*;

public class IdentifierTest {

    /**
     * Проверяет корректное экранирование строки.
     */
    @Test
    public void checkEscapeIdentifier() {

        String stringTestWithNotAllowedCharacters = "This_String_In_Class_\"IdentifierTest\"_Is_For_Test";
        String escapeString = Identifier.getEscapeIdentifier(stringTestWithNotAllowedCharacters);
        String expectedCorrectString = "\"This_String_In_Class_\"\"IdentifierTest\"\"_Is_For_Test\"";
        assertEquals("Корректное экранирование строк", expectedCorrectString, escapeString);

    }

    /**
     * Проверяет на экранирование допустимой строки.
     */
    @Test
    public void checkCorrectIdentifier() {

        String stringTestWithNotAllowedCharacters = "user_name";
        String escapeString = Identifier.getEscapeIdentifier(stringTestWithNotAllowedCharacters);
        String expectedCorrectString = "user_name";
        assertEquals("Корректное экранирование строк", escapeString, expectedCorrectString);

    }

}
