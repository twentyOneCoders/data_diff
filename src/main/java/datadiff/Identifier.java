package datadiff;

/**
 * Проверяет идентификатор на допустимые символы и, при необходимости, экранирует их.
 */
final class Identifier {

    private Identifier() { }

    /**
     * Получить строку с экранированными кавычками, если они присутствуют в идентификаторе.
     *
     * @param diffValue объект, который следует проверить
     * @return объект в виде строки с экранированными кавычками
     */
    static String getEscapeIdentifier(Object diffValue) {

        String valueString = diffValue.toString();
        final String escapedSymbol = "\"";
        if (valueString.contains(escapedSymbol)) {
            valueString = valueString.replace(escapedSymbol, "\"\"");
        }
        return checkStringForNotAllowedCharacters(valueString);

    }

    /**
     * Проверяет строку на допустимые символы.
     * Если в строке присутствуют символы кроме строчных букв, цифр и символа подчеркивания,
     * то заключает строку в двойные кавычки.
     *
     * @param valueString строка, которую нужно проверить
     * @return строка в двойных кавычках, если в ней присутсвуют недопустимые символы
     */
    private static String checkStringForNotAllowedCharacters(String valueString) {

        String allowedCharacters = "abcdefghijklmnopqrstuvwxyz0123456789_";
        StringBuilder valueStringBuilder = new StringBuilder();
        for (int i = 0; i < valueString.length(); i++) {
            char charIdentifier = valueString.charAt(i);
            int charIndex = allowedCharacters.indexOf(charIdentifier);
            if (charIndex == -1) {
                valueStringBuilder.append('"').append(valueString).append('"');
                return valueStringBuilder.toString();
            }
        }
        return valueString;

    }

}
