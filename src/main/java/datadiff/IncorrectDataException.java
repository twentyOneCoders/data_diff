package datadiff;

class IncorrectDataException extends Exception {

    static final long serialVersionUID = -3387516993124229948L;

    IncorrectDataException() { super(); }

    IncorrectDataException(String message) {
        super(message);
    }

    IncorrectDataException(String message, Throwable cause) {
        super(message, cause);
    }

    IncorrectDataException(Throwable cause) {
        super(cause);
    }
}
