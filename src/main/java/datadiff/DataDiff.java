package datadiff;

import java.util.HashMap;
import java.util.Map;

class DataDiff {

    private final Map<Object, Map<String, PairChangedValues>> existRowData = new HashMap<>();

    private final Map<Object, Map<String, Object>> newData = new HashMap<>();

    private final Map<Object, Map<String, Object>> delData = new HashMap<>();

    Map<Object, Map<String, PairChangedValues>> getExistRowData() {
        return existRowData;
    }

    Map<Object, Map<String, Object>> getNewData() {
        return newData;
    }

    Map<Object, Map<String, Object>> getDelData() {
        return delData;
    }

    void addExistRowData(Object id, Map<String, PairChangedValues> columns) {
        existRowData.put(id, columns);
    }

    void addNewData(Object id, Map<String, Object> columns) {
        newData.put(id, columns);
    }

    void addDelData(Object id, Map<String, Object> columns) {
        delData.put(id, columns);
    }

}
