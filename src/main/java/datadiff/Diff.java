package datadiff;

import java.io.PrintWriter;

/**
 * Интерфейс для реализации в классах по выводу нужного формата изменений.
 */
interface Diff {

    /**
     * Получить сформированные изменения.
     *
     * @param printWriter объект класса PrintWriter
     */
    void getDiff(PrintWriter printWriter);

}
