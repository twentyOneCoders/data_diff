package datadiff;

/**
 * Хранит пару измененных значений.
 */
class PairChangedValues {

    private final Object valueFirstTable;
    private final Object valueSecondTable;

    PairChangedValues(Object valueFirstTable, Object valueSecondTable) {
        this.valueFirstTable = valueFirstTable;
        this.valueSecondTable = valueSecondTable;
    }

    Object getValueFirstTable() {
        return valueFirstTable;
    }

    Object getValueSecondTable() {
        return valueSecondTable;
    }

    @Override
    public String toString() {
        return "PairChangedValues{" +
                "valueFirstTable=" + valueFirstTable +
                ", valueSecondTable=" + valueSecondTable +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PairChangedValues that = (PairChangedValues) o;

        if (valueFirstTable != null ? !valueFirstTable.equals(that.valueFirstTable) : that.valueFirstTable != null)
            return false;
        return valueSecondTable != null ? valueSecondTable.equals(that.valueSecondTable) : that.valueSecondTable == null;
    }

    @Override
    public int hashCode() {
        int result = valueFirstTable != null ? valueFirstTable.hashCode() : 0;
        result = 31 * result + (valueSecondTable != null ? valueSecondTable.hashCode() : 0);
        return result;
    }
}
